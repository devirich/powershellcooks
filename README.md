# PowershellCooks

From cookbook to table scraps, /r/PowerShell cooks give their 
favorite dish for your enjoyment. 

# Features
* Database of recipies created by powershellers
* Menu generator (including Random Mode to avoid eating the 
same thing over and over)
* Shopping list generator
* Recipe viewer (instructions)

# Contributing
Got a recipe to share? Awesome. Please follow the following 
guidelines:
1. Only contribute a favorite or two (for now at least). Easiest, 
tastiest, overall favorite, etc.